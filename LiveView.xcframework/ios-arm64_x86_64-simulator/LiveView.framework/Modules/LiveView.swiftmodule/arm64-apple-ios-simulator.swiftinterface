// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.7.2 effective-4.2 (swiftlang-5.7.2.135.5 clang-1400.0.29.51)
// swift-module-flags: -target arm64-apple-ios13-simulator -enable-objc-interop -enable-library-evolution -swift-version 4.2 -enforce-exclusivity=checked -O -module-name LiveView
// swift-module-flags-ignorable: -enable-bare-slash-regex
import Foundation
import QuickLook
import Swift
import SwiftProtobuf
import Swinject
import UIKit
import WebKit
import _Concurrency
import _StringProcessing
import os
@objc @objcMembers public class ActiveUser : ObjectiveC.NSObject {
  @objc final public let id: Swift.String
  @objc public var firstname: Swift.String?
  @objc public var lastname: Swift.String?
  @objc public var email: Swift.String?
  @objc public var tags: [Swift.String]
  @objc public var assignedUser: Swift.String?
  @objc public var externalToken: Swift.String?
  @objc public var additionalTaggingData: [Swift.String : Swift.String]
  @objc public init(id: Swift.String, firstname: Swift.String? = nil, lastname: Swift.String? = nil, email: Swift.String? = nil, tags: [Swift.String] = [], assignedUser: Swift.String? = nil, externalToken: Swift.String? = nil, additionalTaggingData: [Swift.String : Swift.String] = [:])
  @objc deinit
}
@objc public class SetupSharingConfiguration : ObjectiveC.NSObject {
  final public let appGroupId: Swift.String
  public var sessionSharingEnabled: Swift.Bool
  @objc public init(appGroupId: Swift.String)
  @objc deinit
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc public class SetupConfiguration : ObjectiveC.NSObject {
  @objc public init(tenantId: Swift.String, token: Swift.String)
  @objc public func setServerUrl(_ serverUrl: Swift.String)
  @objc public func setCdnUrl(_ cdnUrl: Swift.String)
  @objc public func setSharingConfiguration(_ sharingConfig: LiveView.SetupSharingConfiguration?)
  @objc public func setControlsColor(_ controlsColor: UIKit.UIColor)
  @objc public func setOverlayBackgroundColor(_ overlayBackgroundColor: UIKit.UIColor)
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class Chatvisor : ObjectiveC.NSObject {
  @objc public static var liveView: LiveView.ChatvisorLiveView {
    @objc get
  }
  @objc public static var coBrowsing: LiveView.ChatvisorCoBrowsing {
    @objc get
  }
  @objc public static var webChat: LiveView.ChatvisorWebChat {
    @objc get
  }
  @objc public static var configuration: LiveView.ChatvisorConfiguration {
    @objc get
  }
  @objc public static var user: LiveView.ChatvisorUser {
    @objc get
  }
  @objc public static func setup(with config: LiveView.SetupConfiguration)
  @objc override dynamic public init()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class ChatvisorCoBrowsing : ObjectiveC.NSObject {
  @objc public func start()
  @objc public func start(_ result: @escaping (LiveView.ChatvisorResult) -> Swift.Void)
  @objc public func join(session: LiveView.SharedSession, _ result: @escaping (LiveView.ChatvisorResult) -> Swift.Void)
  @objc public func stop()
  public func connectionState() -> LiveView.ChatvisorConnectionState
  @available(*, deprecated, message: "Please use connectionState() to determine state")
  public func connected() -> Swift.Bool
  @objc public func onConnectionStateChange(callback: LiveView.ConnectionStateResult?)
  @objc public func shareId() -> Swift.String?
  @objc public func setUIEnabled(_ enabled: Swift.Bool)
  @objc public func sharedSession() -> LiveView.SharedSession?
  @objc override dynamic public init()
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers public class ChatvisorConfiguration : ObjectiveC.NSObject {
  @objc public func setCdnServer(hostname: Swift.String)
  @objc public func setServer(hostname: Swift.String)
  @objc deinit
}
@objc public enum ChatvisorConnectionState : Swift.Int, Swift.CustomStringConvertible {
  case disconnected = 0
  case connecting = 1
  case connected = 2
  public var description: Swift.String {
    get
  }
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@_inheritsConvenienceInitializers @objc public class ChatvisorLiveView : ObjectiveC.NSObject {
  public func connectionState() -> LiveView.ChatvisorConnectionState
  @available(*, deprecated, message: "Please use connectionState() to determine state")
  public func connected() -> Swift.Bool
  @objc public func start()
  @objc public func start(_ result: @escaping (LiveView.ChatvisorResult) -> Swift.Void)
  @objc public func join(session: LiveView.SharedSession, _ result: @escaping (LiveView.ChatvisorResult) -> Swift.Void)
  @objc public func stop()
  @objc public func tag(_ tag: Swift.String)
  @objc public func tag(_ tag: Swift.String, _ result: @escaping (LiveView.ChatvisorResult) -> Swift.Void)
  @objc public func setChatOverlayEnabled(isVisible: Swift.Bool)
  @objc public func onConnectionStateChange(callback: LiveView.ConnectionStateResult?)
  @objc public func shareId() -> Swift.String?
  @objc public func sharedSession() -> LiveView.SharedSession?
  @objc override dynamic public init()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class ChatvisorResult : ObjectiveC.NSObject {
  public var success: Any? {
    get
  }
  public var failure: Swift.Error? {
    get
  }
  convenience public init<Success, Failure>(_ result: Swift.Result<Success, Failure>) where Failure : Swift.Error
  @objc override dynamic public init()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class ChatvisorUser : ObjectiveC.NSObject {
  @objc public func tag(_ user: LiveView.ActiveUser)
  @objc public func tag(_ user: LiveView.ActiveUser, _ result: @escaping (LiveView.ChatvisorResult) -> Swift.Void)
  @objc public func clear()
  @objc override dynamic public init()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class ChatvisorWebChat : ObjectiveC.NSObject {
  @objc public func open()
  @objc public func open(_ result: @escaping (LiveView.ChatvisorResult) -> Swift.Void)
  @objc public func open(for tenantId: Swift.String)
  @objc public func open(for tenantId: Swift.String, _ result: @escaping (LiveView.ChatvisorResult) -> Swift.Void)
  @objc public func close()
  @objc override dynamic public init()
  @objc deinit
}
public enum Injection {
  public static func getContainer() -> Swinject.Container
}
public typealias ConnectionStateResult = (_ state: LiveView.ChatvisorConnectionState) -> Swift.Void
@objc @_hasMissingDesignatedInitializers public class SharedSession : ObjectiveC.NSObject, Swift.Codable {
  final public let sessionId: Swift.String
  final public let shareId: Swift.String
  final public let initiatorBundleId: Swift.String
  final public let initiatorName: Swift.String
  final public let startedAt: Foundation.Date
  @objc override dynamic public func isEqual(_ object: Any?) -> Swift.Bool
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
  required public init(from decoder: Swift.Decoder) throws
}
extension LiveView.ChatvisorConnectionState : Swift.Equatable {}
extension LiveView.ChatvisorConnectionState : Swift.Hashable {}
extension LiveView.ChatvisorConnectionState : Swift.RawRepresentable {}
